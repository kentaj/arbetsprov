import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
var Star = require('react-icons/lib/fa/star');

@inject('FavoriteListStore')
@observer

class FavoriteList extends Component {

  static propTypes = {
    FavoriteListStore: PropTypes.object
  }

  handleRemoveFavorite(index, e) {
    const { FavoriteListStore } = this.props;
    FavoriteListStore.removeFavorite(index);
  }

  buildFavoriteList() {
        const { FavoriteListStore: { page }, FavoriteListStore } = this.props;
        const start = (page - 1) * 10;
        const end = page * 10;
        const favoriteSubset = FavoriteListStore.favoriteArray.slice(start, end);

        return (
            <ul>
            {favoriteSubset.map((favorite, index) => (
               <li key={favorite} id={favorite}>
                    {favorite}     
                    <div className="button-container">
                        <div 
                            className="delete-button star" 
                            onClick={(e) => this.handleRemoveFavorite(index, e)} >
                            <Star/>
                        </div>
                    </div>
                </li>
            ))}
            </ul>
        )
    }

    buildPaginator() {
        const { page, favoriteCount, nextPage, previousPage } = this.props.FavoriteListStore;
        const prevOn = page !== 1; 
        const nextOn = favoriteCount > page * 10;

        return (
            <div className="pagination">
                <button 
                    disabled={!prevOn}
                    className="prev"
                    onClick={previousPage}
                >
                    <span>Previous page</span>
                </button>
                <button 
                    disabled={!nextOn}
                    className="next"
                    onClick={nextPage}
                >
                    <span>Next page</span>
                </button>
            </div>

        )
    }

    render() {
            const count = this.props.FavoriteListStore.favoriteCount;
              return(
                <div className="favorite-container box">
                      <h2 className="title">My Favorites {  count  } <Star /> </h2>
                      <div className='favorite-list'>
                            {this.buildFavoriteList() }
                      </div>
                      {(count > 10) ? this.buildPaginator() : null}
                </div>
            );
      }
}

export default FavoriteList;
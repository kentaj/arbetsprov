import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
var Star = require('react-icons/lib/fa/star');
var Trash = require('react-icons/lib/fa/trash');

@inject('CountryListStore', 'FavoriteListStore')
@observer
class CountryList extends Component {

    static propTypes = {
        CountryListStore: PropTypes.object,
        FavoriteListStore: PropTypes.object
    }

    handleAddFavorite = (country) => {
        const { FavoriteListStore } = this.props;
        FavoriteListStore.addFavorite(country);
        console.log("Added favorite "+ country)
    }

    handleRemoveCountry = (country) => {
        const { CountryListStore, FavoriteListStore } = this.props;
        CountryListStore.removeCountry(country);
        FavoriteListStore.removeFavorite(country);
        console.log("Removed country "+ country)
    }

    handleInput = (e) => {
        const { CountryListStore: { inputText, shouldSuggest, setSuggestions, googleService }, CountryListStore } = this.props;
        CountryListStore.setInputText(e.target.value);

        if (inputText.length > 1 && shouldSuggest) {
            googleService.getPlacePredictions({
                types: ['(regions)'],
                input: inputText
            },
            (predictions = []) => {
                const suggestedCountryIndexArray = predictions ? 
                predictions.filter(
                    suggestion => suggestion.types.indexOf("country") >= 0
                ) : [];

              setSuggestions(suggestedCountryIndexArray);
            });
        } else {
          setSuggestions([]);
        }
    }

    buildCountryList() {
        const { CountryListStore: { page }, CountryListStore } = this.props;
        const start = (page - 1) * 10;
        const end = page * 10;
        const countrySubset = CountryListStore.countryArray.slice(start, end);

        return (
            <ul>
            {countrySubset.map((country, index) => (
               <li key={country} id={country}>
                    {country}     
                    <div className="button-container">

                        <div className="delete-button" 
                             onClick={() => this.handleRemoveCountry(country)} >
                             <Trash/>
                        </div>
        
                        <div className="favorite-button"
                             onClick={() => this.handleAddFavorite(country)}>
                             <Star />
                        </div>

                    </div>
                </li>
            ))}
            </ul>
        )
    }

    buildPaginator() {
        const { page, countryCount, nextPage, previousPage } = this.props.CountryListStore;
        const prevOn = page !== 1; 
        const nextOn = countryCount > page * 10;

        return (
            <div className="pagination">
                <button 
                    disabled={!prevOn}
                    className="prev"
                    onClick={previousPage}
                >
                    <span>Previous page</span>
                </button>
                <button 
                    disabled={!nextOn}
                    className="next"
                    onClick={nextPage}
                >
                    <span>Next page</span>
                </button>
            </div>

        )
    }

    render() { 
        const { validCountry, countryCount, inputText, suggestions, addSuggestion, addCountry } = this.props.CountryListStore;

        return(
            <div className="country-container box">
                <h2 className="title">My countries { countryCount } </h2>

                <form>
                    <input
                        ref='countryInput'
                        type="text" 
                        placeholder="Search for country" 
                        value={inputText}
                        onChange={this.handleInput}
                    />
                    <div className='country-dropdown'>
                        <ul>
                            {suggestions.map((country, index) => <li onClick={() => addSuggestion(country)} key={index}>{country.description}</li> )}
                        </ul>
                    </div>
                    <button 
                        disabled={!validCountry}
                        onClick={addCountry}
                    >
                        <span>Add +</span>
                    </button>
                </form>
                <div className='country-list'>
                    {this.buildCountryList() }
                </div>
                {(countryCount > 10) ? this.buildPaginator() : null}
            </div>
        );
    }
}

export default CountryList;

import request from 'superagent';
import config from './config';

export const getCountrySuggestions = async (text) => {	
	return await request.get('https://maps.googleapis.com/maps/api/place/autocomplete/json')
		.query({input: text})
		.query({types: "(countries)"})
		.query({key: config.googleApiKey})
}

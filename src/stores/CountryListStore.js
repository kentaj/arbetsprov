import {observable, action, computed} from 'mobx';

class CountryListStore {
	  @observable countryArray = [];
	  @observable favoriteArray = [];
	  @observable validCountry= false;
	  @observable inputText='';
	  @observable suggestedCountry= '';
	  @observable shouldSuggest= true;
	  @observable page = 1;
  	@observable suggestions = [];
  	@observable googleService = null;

  	constructor () {
    	this.googleService = new window.google.maps.places.AutocompleteService();
  	}

  	@action
  	setInputText = (text) => {
    	this.validCountry = false;
    	this.inputText = text;
  	}

  	@action
  	setSuggestedCountry = (country) => {
    	this.suggestedCountry = country;
  	}

  	@action
  	setSuggestions = (suggestions) => {
    	this.suggestions = suggestions;
  	}

  	@action
  	setShouldSuggest = (bool) => {
    	this.shouldSuggest = bool;
  	}

  	@action
  	setValidCountry = (bool) => {
    	this.validCountry = bool;
  	}

  	@action
  	nextPage = () => {
    	this.page++;
  	}

  	@action
  	previousPage = () => {
    	this.page--;
  	}

  	@action 
  	addCountry = () => {
		  this.countryArray.push(this.suggestedCountry.description);
    	this.suggestions = [];
    	this.inputText = '';
    	this.validCountry = false;
    	this.suggestedCountry = null;
	  }

  	@action
  	addSuggestion = (country) => {
    	this.suggestedCountry = country;
    	this.validCountry = true;
    	this.inputText = country.description;
  	}

	  @action removeCountry = (country) => {
    	const index = this.countryArray.indexOf(country);
		  this.countryArray.splice(index, 1);
	  }

	  @computed get countryCount() {
		  return this.countryArray.length;
	  }

	  @action removeFavorite = (country) => {
      const index = this.favoriteArray.indexOf(country);
		  this.favoriteArray.splice(index, 1);
	  }

}

const store = new CountryListStore();
export default store;

import {observable, action, computed} from 'mobx';

class FavoriteListStore {
	@observable favoriteArray = [];
	@observable page = 1;

	@action addFavorite = (country) => {
		this.favoriteArray.push(country);
	}

	@action
  	nextPage = () => {
    	this.page++;
  	}

  	@action
  	previousPage = () => {
    	this.page--;
  	}

	@action removeFavorite = (country) => {
      	const index = this.favoriteArray.indexOf(country);
		this.favoriteArray.splice(index, 1);
	}
	
	@computed get favoriteCount() {
		return this.favoriteArray.length;
	}

}

const store = new FavoriteListStore();
export default store;

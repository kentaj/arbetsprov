import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'mobx-react';
import CountryListStore from './stores/CountryListStore';
import FavoriteListStore from './stores/FavoriteListStore';



const Root = (
	<Provider FavoriteListStore={FavoriteListStore} CountryListStore={CountryListStore}>
		<App />
	</Provider>
);

ReactDOM.render(Root , document.getElementById('root'));
registerServiceWorker();

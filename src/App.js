import React, { Component } from 'react';
import CountryList from './components/CountryList'
import FavoriteList from './components/FavoriteList'

import './index.css';

class App extends Component {

  render() {
    

    return (
      <div className="App">
      	<div className="container">
        	<CountryList></CountryList>
        	<FavoriteList></FavoriteList>
        </div>
      </div>
    );
  }
}

export default App;
